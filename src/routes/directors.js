const express = require('express');

const router = express.Router();
const pool = require('../utils/connection.js');
const { directorsValidator } = require('../middlewares/schemasValidator');
const { handleError } = require('../middlewares/errorHandler');

router.get('', (req, res, next) => {
  const sql = 'select * from directors limit 10';
  pool
    .query(sql, [])
    .then((result) => {
      res.status(200).json({ success: true, msg:'', result });
    })
    .catch((err) => {
      next(err);
    });
});

router.get('/detail', (req, res, next) => {
  const sql = `SELECT d.id, director, GROUP_CONCAT(Title) movies,GROUP_CONCAT(CONVERT(m.id,char(8))) movieId, count(Title) movieCount
 FROM film.directors d left join film.movies m on m.DirectorId =d.id group by d.id order by director
 limit 10`;
  pool
    .query(sql, [])
    .then((result) => {
      res.status(200).json({ success: true, msg:'', result });
    })
    .catch((err) => {
      next(err);
    });
});

router.get('/:directorId', (req, res, next) => {
  const sql = 'select * from directors where id= ?';
  pool
    .query(sql, [req.params.directorId])
    .then((result) => {
      res.status(200).json({ success: true, msg:'', result });
    })
    .catch((err) => {
      next(err);
    });
});

router.put('/:directorId', directorsValidator, (req, res, next) => {
  const sql = 'update directors set ? where id=?';
  pool
    .query(sql, [{ director: req.body.director }, req.params.directorId])
    .then((result) => {
      res.status(200).json({ success: true, msg:'', result });
    })
    .catch((err) => {
      next(err);
    });
});

router.delete('/:directorId', (req, res, next) => {
  const sql = 'delete from directors where id=?';
  pool
    .query(sql, [req.params.directorId])
    .then((result) => {
      res.status(200).json({ success: true, msg:'', result });
    })
    .catch((err) => {
      next(err);
    });
});

router.post('', directorsValidator, (req, res, next) => {
  const sql = 'insert into directors set ?';
  pool
    .query(sql, [{ director: req.body.director }])
    .then((result) => {
      res.status(200).json({ success: true, msg:'', result });
    })
    .catch((err) => {
      next(err);
    });
});

router.use(handleError);
module.exports = router;

// 'id','Rank','Title','Description','Runtime','Genre','Rating','Metascore','Votes',
// 'Gross_Earning_in_Mil','DirectorId','Actor','Year'

// https://github.com/hapijs/joi/blob/v14.3.1/API.md#list-of-errors
