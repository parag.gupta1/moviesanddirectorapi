const express = require('express');

const router = express.Router();
const pool = require('../utils/connection.js');
const { moviesValidator } = require('../middlewares/schemasValidator');
const { handleError } = require('../middlewares/errorHandler');

router.get('', (req, res) => {
  const sql = `select movies.id id, Rank,Title,Description,Runtime,Genre,Rating,Metascore,Votes,Gross_Earning_in_Mil,DirectorId,Actor,Year,director
 from directors inner join movies on movies.directorId=directors.id limit 10`;
  pool
    .query(sql, [])
    .then((result) => {
      res.status(200).json({ success: true, msg: '', result });
    })
    .catch((err) => {
      next(err);
    });
});

router.get('/:movieId', (req, res) => {
  const sql = `select * from movies inner join directors on movies.directorId=directors.id where movies.id=${req.params.movieId}`;
  pool
    .query(sql, [])
    .then((result) => {
      res.status(200).json({ success: true, msg: '', result });
    })
    .catch((err) => {
      next(err);
    });
});

// give all keys
router.put('/:movieId', (req, res) => {
  const sql = 'update movies set ? where id=?';
  const sqlData = [];
  const updateData = {
    Title: req.body.title,
    Description: req.body.description,
    Rank: req.body.rank,
    Runtime: req.body.runtime,
    Genre: req.body.genre,
    Rating: req.body.rating,
    Metascore: req.body.metascore,
    Votes: req.body.votes,
    Gross_Earning_in_Mil: req.body.grossEarningInMil,
    DirectorId: req.body.directorId,
    Actor: req.body.actor,
    Year: req.body.year,
  };
  sqlData[0] = updateData;
  sqlData[1] = req.params.movieId;

  pool
    .query(sql, sqlData)
    .then((result) => {
      res.status(200).json({ success: true, msg: '', result });
    })
    .catch((err) => {
      next(err);
    });
});

router.delete('/:movieId', (req, res) => {
  const sql = 'delete from movies where id=?';
  pool
    .query(sql, [req.params.movieId])
    .then((result) => {
      res.status(200).json({ success: true, msg: '', result });
    })
    .catch((err) => {
      next(err);
    });
});

router.post('', moviesValidator, (req, res) => {
  const sql = 'insert into movies set ?';
  const addData = [
    {
      Title: req.body.title,
      Description: req.body.description,
      Rank: req.body.rank,
      Runtime: req.body.runtime,
      Genre: req.body.genre,
      Rating: req.body.rating,
      Metascore: req.body.metascore,
      Votes: req.body.votes,
      Gross_Earning_in_Mil: req.body.grossEarningInMil,
      DirectorId: req.body.directorId,
      Actor: req.body.actor,
      Year: req.body.year,
    },
  ];
  pool
    .query(sql, addData)
    .then((result) => {
      res.status(200).json({ success: true, msg: '', result });
    })
    .catch((err) => {
      next(err);
    });
});

router.use(handleError);
module.exports = router;

// 'id','Rank','Title','Description','Runtime','Genre','Rating','Metascore','Votes',
// 'Gross_Earning_in_Mil','DirectorId','Actor','Year'
