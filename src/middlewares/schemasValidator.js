const Joi = require('joi');
// const anySchema = Joi.any();

const movieSchema = Joi.object().keys({
  // id: Joi.number().integer().positive().required(),
  rank: Joi.number()
    .positive()
    .integer()
    .required(),
  // .error(errors => {console.log(errors);
  //   return errors.details.message}),
  title: Joi.string()
    .min(2)
    .required(),
  description: Joi.string().min(25),
  runtime: Joi.number()
    .positive()
    .integer(),
  genre: Joi.string()
    .required()
    .valid([
      'Crime',
      'Action',
      'Biography',
      'Adventure',
      'Western',
      'Drama',
      'Animation',
      'Comedy',
      'Horror',
      'Mystery',
    ]),
  rating: Joi.number()
    .positive()
    .greater(1)
    .max(10)
    .precision(2)
    .required(),
  metascore: Joi.number()
    .integer()
    .min(0)
    .max(100)
    .required(),
  votes: Joi.number()
    .positive()
    .integer()
    .required(),
  gross_Earning_in_Mil: Joi.number()
    .positive()
    .greater(1)
    .precision(2),
  directorId: Joi.number()
    .positive()
    .integer()
    .required(),
  actor: Joi.string().required(),
  year: Joi.number()
    .integer()
    .min(1900)
    .max(2020)
    .required()
    .error((errors) => {
      errors.forEach((err) => {
        switch (err.type) {
          case 'any.empty':
            err.message = 'Year should not be empty!';
            break;
          case 'any.required':
            err.message = 'Year should be required!';
            break;
          case 'number.min':
            err.message = `Year should have at least ${err.context.limit} !`;
            break;
          case 'number.max':
            err.message = `Year should have at most ${err.context.limit} !`;
            break;
          default:
            break;
        }
      });
      return errors;
    }),
});

const directorSchema = Joi.object().keys({
  director: Joi.string()
    .min(2)
    .required(),
});

function moviesValidator(req, res, next) {
  Joi.validate(req.body, movieSchema, (err, value) => {
    if (err) {
      // console.log(err);
      res.status(422).json({
        status: 'error',
        message: 'Invalid request data',
        result: err.details[0].message,
      });
    } else {
      next();
    }
  });
}
function directorsValidator(req, res, next) {
  // console.log("************88");
  Joi.validate(req.body, directorSchema, (err, value) => {
    if (err) {
      // console.log(err);
      res.status(422).json({
        status: 'error',
        message: 'Invalid request data',
        result: err.details[0].message,
      });
    } else {
      next();
    }
  });
}

module.exports = { moviesValidator, directorsValidator };

// https://github.com/hapijs/joi/blob/v14.3.1/API.md#list-of-errors
