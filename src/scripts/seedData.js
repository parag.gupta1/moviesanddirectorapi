const pool = require('../utils/connection.js');
const myJsonData = require('./movie.json');

const directorData = {};
function getDirectorId(name) {
  return directorData[name];
}

function configureDatabase() {
  const createTablesSql = `DROP TABLE IF EXISTS movies;DROP TABLE IF EXISTS directors;
    create table directors (id int primary key auto_increment, director  varchar(100) unique);    
    CREATE TABLE movies(id int primary key auto_increment,   
   Rank varchar(255),
   Title varchar(255),
   Description varchar(255),
   Runtime int(11),
   Genre varchar(255),
   Rating decimal(10,1),
   Metascore int(11),
   Votes varchar(255),
   Gross_Earning_in_Mil decimal(10,2), 
   DirectorId varchar(255),
   Actor varchar(255),
   Year varchar(4),FOREIGN KEY (DirectorId) REFERENCES directors(id))`;
  const insertDataSql = 'insert ignore into directors(director) VALUES ?';
  const getDirectorDataSql = 'select * from directors ';
  const insertDataSql2 = 'insert ignore into movies VALUES ?';

  pool
    .query(createTablesSql)
    .then(() => pool.query(insertDataSql, [myJsonData.map(movie => [movie.Director])]))
    .then(() => pool.query(getDirectorDataSql))
    .then((result) => {
      result.forEach((director) => {
        directorData[director.director] = director.id;
      });
      pool.query(insertDataSql2, [
        myJsonData.map((movie) => {
          const directorId = getDirectorId(movie.Director);
          return [
            0,
            movie.Rank,
            movie.Title,
            movie.Description,
            movie.Runtime,
            movie.Genre,
            movie.Rating,
            movie.Metascore,
            movie.Votes,
            movie.Gross_Earning_in_Mil,
            directorId,
            movie.Actor,
            movie.Year,
          ];
        }),
      ]);
    })
    .then(() => {
      console.log('Table configuration is done');
    })
    .catch((err) => {
      console.error(`There is error ${err}`);
    });
}


module.exports = configureDatabase();
